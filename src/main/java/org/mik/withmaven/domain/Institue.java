package org.mik.withmaven.domain;
/**
 * @author Daniyar Serikov KFOHLK
 *
 */

public enum Institue {
	
	/**
	 * 
	 * Institue that can be used :  INFORMATICS,MECHANICS , AUTOMATION
	 */
	
	INFORMATICS,MECHANICS , AUTOMATION

}
