package org.mik.withmaven.domain;
/**
 * @author Daniyar Serikov KFOHLK
 *
 */
public class Student extends AbstractPerson {
	

	/**
	 * adding  private member :specialization
	 */
	
	private Specialization specialization;
	
	/**
	 * Generating constructor using fields 
	 @param name name of Student
	 @param year year of Student
	 @param specialization specialization of Student
	 * 
	 */

	public Student(String name, int year, Specialization specialization) {
		super(name,year);
		this.specialization = specialization;
	}
	
	public boolean isStudent() {
		return true;
		
	}
	
	/** 
	 * Generating code :  getter and setter  for Specialization 
	 @return with Specialization
	 * */

	public Specialization getSpecialization() {
		return this.specialization;
	}

	public void setSpecialization(Specialization specialization) {
		this.specialization = specialization;
	}
	
	/** 
	 * Generating code : hash code and equals for Specialization
	 * */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((specialization == null) ? 0 : specialization.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (specialization != other.specialization)
			return false;
		return true;
	}
	
	/** 
	 * 
	 * Overriding the toString() method of Student.java
	 * */
	
	@Override
	public String toString() {
	
		return "Student: " + super.toString();
	}

}
