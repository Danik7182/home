package org.mik.withmaven.domain;

/**
 * @author Daniyar Serikov KFOHLK
 *
 */

public enum Specialization {
	
	/** *
	 * 
	 * Specializations that can be used : INFORMATICS, ELECTRIC , MECHANICS, ARCHITECT 
	 */
	
	INFORMATICS, ELECTRIC , MECHANICS, ARCHITECT 

}
