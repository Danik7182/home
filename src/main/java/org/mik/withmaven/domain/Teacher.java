package org.mik.withmaven.domain;

/**
 * @author Daniyar Serikov KFOHLK
 *
 */
public class Teacher extends AbstractPerson {
	
	/**
	 * adding  private member : institue 
	 */
	
	
	private Institue institute;
	
	

	
	/**
	 * Generating constructor using fields 
	 @param name name of Teacher
	 @param birthYear birthYear of Teacher
	 @param inst Institue of Teacher
	 */
	
	public Teacher(String name, int birthYear, Institue inst) {
		super(name, birthYear);
		this.institute = inst;
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * 
	 * 
	 * @return with insitue 
	 */
	public Institue getInstitute() {
		return institute;
	}

	public void setInstitute(Institue institute) {
		this.institute = institute;
	}
	
	/** 
	 * Generating code : hash code and equals for institute
	 * */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((institute == null) ? 0 : institute.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Teacher other = (Teacher) obj;
		if (institute != other.institute)
			return false;
		return true;
	}
	
	/** 
	 * Overriding the toString() method of Teacher.java
	 * 
	 * */
	
	@Override
	public String toString() {
		
		return "Teacher: " + super.toString();
	}

}
