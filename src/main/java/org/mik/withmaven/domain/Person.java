package org.mik.withmaven.domain;

/**
 * @author Daniyar Serikov KFOHLK
 *
 */
public interface Person {
	
	/**
	 * 
	 *method descriptions for getName, 
	 @return with name
	 */

	String getName();
	
	/**
	 * 
	 *method descriptions for setName, 
	 *@param name name of the student
	 */

	void setName(String name);

	/**
	 * 
	 * @return year of birth
	 */
	int getBirthYear();

	/**
	 * 
	 * @param year year of birth
	 */
	void setBirthYear(int year);
	
	/**
	 * 
	 * @return is it a student
	 */
	boolean isStudent();
	
		/**
		 * 
		 * @return true if the person is a teacher
		 */
	boolean isTeacher();
	
	

}
