package org.mik.withmaven;

import org.mik.withmaven.domain.Institue;
import org.mik.withmaven.domain.Specialization;
import org.mik.withmaven.domain.Student;
import org.mik.withmaven.domain.Teacher;

/**
 * @author Daniyar Serikov KFOHLK
 *
 */
public class App 
{
    public static void main( String[] args )
    
    {
    	/**
    	 * Creating instances (st and st1 ) with name, birthYear,Specialization/Institue from the  class and printing values with System.out.println();
    	 * 
    	 */
    	 Student st = new Student("Daniyar ", 2000, Specialization.MECHANICS); //$NON-NLS-1$
         
                
         Teacher st1 = new Teacher("Damir Seifulla", 2001, Institue.MECHANICS); //$NON-NLS-1$
         
         
         System.out.println(st);

         
         System.out.println(st1);
      
    }
}
