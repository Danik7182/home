package org.mik.withmaven;


/**
 * @author Daniyar Serikov KFOHLK
 *
 */

import org.junit.runner.RunWith;
import org.junit.runners.Suite;




@RunWith(Suite.class)
@Suite.SuiteClasses ({
	DomainTest.class
})

public class AppTest{
	/**
	 * empty block
	 */
	
	
}



